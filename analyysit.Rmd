---
title: "Mahtavat geenianalyysit"
author: "Risto Kaartinen"
date: "8/19/2019"
output: html_document
---



```{r echo=FALSE, message=FALSE, warning=FALSE}
## try http:// if https:// URLs are not supported
# if (!requireNamespace("BiocManager", quietly=TRUE))
#   install.packages("BiocManager")
#BiocManager::install("enrichplot")
# BiocManager::install("enrichplot")
# BiocManager::install('EnhancedVolcano')
#BiocManager::install('org.Hs.eg.db')
#BiocManager::install('DESeq2')


library(clusterProfiler)
library(DOSE)
library(readxl)
library(tidyverse)
library(enrichplot)
library(org.Hs.eg.db)
library(DT)
library(DESeq2)
# tähän joku kommentti
```

Projektin koodit: https://gitlab.com/dataninjafi/bioconductor_jeesi

# Eka datasetti

```{r}

analysis <- read_excel("data/geenit.xlsx") %>% 
  pull(`Gene ID`) %>% 
  enrichGO(OrgDb = "org.Hs.eg.db", ont = "ALL")  

summary(analysis) %>% 
  mutate(indeksi = row_number()) %>% 
  datatable
  
```


```{r}
teeDotplot <- function(analysis, path = tempfile()){
  
  #tiff(filename = path, units="in", width=5, height=5, res= 300)
  # dotplot(analysis, showCategory=20, font.size = 7) +  
  #   #scale_color_gradient(low="blue", high="red") +
  #   scale_fill_continuous(limits = c(0,20), breaks = c(0, 5, 10, 15, 20)) 
  #   theme(plot.title = element_text(size = 7, face = "bold"),
  #   legend.title=element_text(size=7), 
  #   legend.text=element_text(size=7))
  
  analysis %>% 
  summary %>% 
  mutate(p.adjust = case_when(
    p.adjust < 0.00001 ~ "p < 0.0001",
    p.adjust < 0.0001 ~ "p < 0.0001",
    p.adjust < 0.001 ~ "p < 0.001",
    p.adjust < 0.01 ~ "p < 0.01",
    p.adjust < 0.05 ~ "p < 0.05"
  )) %>%
  mutate(GeneRatio = as.numeric(str_extract(GeneRatio, "^\\d*(?=\\/)")) / as.numeric(str_extract(GeneRatio, "(?<=\\/)\\d*"))) %>% 
  arrange(desc(GeneRatio)) %>% 
  dplyr::slice(1:20) %>% 
  mutate(Description = paste(ID, Description),
         Description = fct_reorder(Description, GeneRatio)) %>% 
  ggplot()+ aes(GeneRatio, Description, color = p.adjust, size = Count, label = Count) + 
  geom_point() +
  ylab("") +
  scale_color_manual(values=rev(c("#feedde", "#fdbe85", "#fd8d3c", "#d94701"))) +
  geom_text(color = "black", size = 2)


  #dev.off()
  
  # dotplot(analysis, showCategory=20) + 
  # scale_color_gradient(low="blue", high="red") 
  ggsave(filename = path, device = "tiff", width = 5, height = 5, dpi = 300, units = "in")
}


  
  
  

teeDotplot(analysis, "pics/kuva1.tiff")
```


# 2. Datasetti

```{r}
analysis <- read.csv("data/geenit3.csv") %>% 
  pull(Gene.ID) %>% 
  enrichGO(OrgDb = "org.Hs.eg.db", ont = "ALL")

summary(analysis) %>% 
  mutate(indeksi = row_number()) %>% 
  datatable()

teeDotplot(analysis, "pics/kuvat2.tiff")



```


  
